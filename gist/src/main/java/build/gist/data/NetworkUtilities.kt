package build.gist.data

class NetworkUtilities {
    companion object {
        internal const val ORGANIZATION_ID_HEADER = "X-Bourbon-Organization-Id"
        internal const val USER_TOKEN_HEADER = "X-Gist-User-Token"
    }
}